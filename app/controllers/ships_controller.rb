class ShipsController < ApplicationController

  def index
    @ships = Ship.all
  end

  def show
    @ship = Ship.find(params[:id])
  end

  def new
    @ship = Ship.new
  end

  def edit
    @ship = Ship.find(params[:id])
  end

  def create
    @ship = Ship.new(ship_params)
    if @ship.save
      redirect_to @ship
    else
      render 'new'
    end
  end

  def update
    @ship = Ship.find(params[:id])

    #you can see why we defined the private
    #ship_params method to keep code DRY
    if @ship.update(ship_params)
      redirect_to @ship
    else
      render 'edit'
    end
  end

  def destroy
    @ship = Ship.find(params[:id])
    @ship.destroy

    redirect_to ships_path
  end

  private

  def ship_params
    params.require(:ship).permit(:name, :size)
  end
end
