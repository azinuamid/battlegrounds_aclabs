class Ship < ApplicationRecord
  validates :name, presence: true, length: { minimum: 5 }
  validates_numericality_of :size
end
