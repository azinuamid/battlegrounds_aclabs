class CreateShips < ActiveRecord::Migration[5.0]
  def change
    create_table :ships do |t|
      t.integer :size
      t.text :name

      t.timestamps
    end
  end
end
